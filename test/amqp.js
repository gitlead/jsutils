const AMQP = require('../amqp');
const { expect } = require('chai');

describe('jsutils/amqp', () => {
  let amqp = null;

  it('should create AMQP instance', () => {
    amqp = new AMQP(['amqp://localhost']);
    expect(amqp).to.be.not.null;
    expect(amqp).to.be.an.instanceof(AMQP);
  });

  it('should subscribe to the queue and receive msg', (done) => {
    const text = 'test msg';

    amqp.queue('testq', (msg) => {
      expect(msg.toString()).to.equal(text);
      done();
    });

    amqp.sendToQueue('testq', text);
  });

  it('should subscribe to the broadcast queue and receive msg', (done) => {
    const text = 'test msg';
    let num = 0;
    const amqp2 = new AMQP(['amqp://localhost']);

    amqp.subscribe('tests', (msg) => {
      expect(msg.toString()).to.equal(text);
      num++;
      if (num === 2) done();
    });

    amqp2.subscribe('tests', (msg) => {
      expect(msg.toString()).to.equal(text);
      num++;
      if (num === 2) done();
    });

    setTimeout(() => {
      amqp.publish('tests', text);
    }, 500);
  });
});

