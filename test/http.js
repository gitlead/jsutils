const HttpClient = require('../http');
const { expect } = require('chai');
const http = require('http');

const server = (func, port) => {
  const srv = http.createServer(func);
  srv.listen(port);
  return srv;
};

describe('jsutils/http', () => {
  it('should work', (done) => {
    server((req, res) => {
      expect(req.method).to.equal('GET');
      expect(req.url).to.equal('/hello');
      res.setHeader('Content-Type', 'application/json');
      res.write(JSON.stringify({ hello: 'world' }));
      res.end();
    }, 23456);

    const client = new HttpClient([`http://localhost:${23456}`]);
    client.get('/hello')
      .then((res) => {
        expect(res).to.be.an('object');
        expect(res.hello).to.equal('world');
        done();
      })
      .catch((err) => done(err));
  });
});

