const url = require('url');
const http = require('http');
const https = require('https');

class HttpClient {
  constructor(servers) {
    if (!servers.length) {
      throw new Error('expecting at least one server');
    }

    this.servers = servers.map((server) => url.parse(server));
    this.activeServer = 0;
    this.log = (msg) => console.error(msg);
  }

  setLogger(func) {
    this.log = func;
  }

  requestToServer(server, method, path, body = '') {
    return new Promise((resolve, reject) => {
      const module = server.protocol === 'https:' ? https : http;
      const req = module.request(Object.assign({}, server, {
        method,
        path,
        headers: {
          'Accept': 'application/json'
        },
        timeout: 5000,
      }), (res) => {
        let data = new Buffer('');
        res.on('data', (chunk) => { data = Buffer.concat([data, chunk]); });
        res.on('end', () => {
          try {
            const json = JSON.parse(data.toString());
            resolve(json);
          } catch (err) { reject(err); }
        });
      });

      req.on('error', (err) => reject(err));

      if (body) {
        req.write(typeof(body) === 'string' ? body : JSON.stringify(body))
      }

      req.end(); // starts
    });
  }

  request(method, path, body = '') {
    return new Promise((resolve, reject) => {
      (function run(count = 0) {
        if (count >= this.servers.length * 3) {
          return reject(new Error('too many retries'));
        }

        this.requestToServer(this.servers[this.activeServer], method, path, body)
          .then((data) => {
            resolve(data);
          })
          .catch((err) => {
            this.log(err.message);
            this.activeServer = ++this.activeServer % this.servers.length;
            setTimeout(run.bind(this, count + 1), 500);
          });
      }).call(this);
    });
  }

  get(url, query) {
    let path = url;
    const queryToString = (q) => Object.keys(q).filter((k) => k && q[k]).map((k) => `${k}=${q[k]}`).join('&');

    if (query) {
      if (path.indexOf('?') !== -1) {
        path = `${path}&${queryToString(query)}`;
      } else {
        path = `${path}?${queryToString(query)}`;
      }
    }

    return this.request('GET', path);
  }

  post(url, body) {
    return this.request('POST', url, body);
  }
}

HttpClient.default = HttpClient; // ES6 modules support
module.exports = HttpClient;

