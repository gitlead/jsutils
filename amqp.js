const EventEmitter = require('events');
const amqplib = require('amqplib/callback_api');

class AMQP extends EventEmitter {
  constructor(servers) {
    super();

    if (!Array.isArray(servers)) {
      throw TypeError('expecting first argument to be an array');
    }

    if (servers.length <= 0) {
      throw new Error('expecting one server at least');
    }

    this.servers = servers;
    this.connection = null;
    this.channel = null;
    this.reconnectTime = -1;
    this.assertedQueues = [];
    this.assertedExchanges = [];
    this.queueListeners = [];
    this.subscribeListeners = [];
    this.log = (type, ...msg) => (type === 'error' ? console.error : console.log).call(console, type, ...msg);

    this.createStableConnection();
  }

  setLogger(func) {
    this.log = func;
  }

  connect() {
    return new Promise((resolve, reject) => {
      try {
        if (this.serverId === undefined) {
          this.serverId = 0;
        } else {
          this.serverId = ++this.serverId % this.servers.length;
        }

        amqplib.connect(this.servers[this.serverId], (err, conn) => {
          if (err) return reject(err);
          this.connection = conn;
          conn.createChannel((err, chan) => {
            if (err) return reject(err);
            this.channel = chan;
            resolve();
          });
        });
      } catch (err) { reject(err); }
    });
  }

  createStableConnection() {
    this.connect()
      .then(() => {
        this.log('info', 'got connection');
        this.reconnectTime = -1;
        this.assertedQueues = [];
        this.assertedExchanges = [];
        this.queueListeners.forEach((q) => this.addQueueToConn(q[0], q[1]));
        this.subscribeListeners.forEach((q) => this.addSubscribeToConn(q[0], q[1]));
        this.emit('connected');
        this.connection.on('error', (err) => this.log('error', err));
        this.connection.on('close', this.onClose.bind(this));
      })
      .catch((err) => {
        this.log('error', `error on connect: ${err.message}`);
        if (this.reconnectTime === -1) {
          this.reconnectTime = 0;
        } else if (this.reconnectTime === 0) {
          this.reconnectTime = 1000;
        } else {
          this.reconnectTime *= 2;
        }
        if (this.reconnectTime > 10000) {
          this.reconnectTime = 10000;
        }
        setTimeout(this.createStableConnection.bind(this), this.reconnectTime);
      });
  }

  onClose() {
    this.createStableConnection();
  }

  addQueueToConn(name, listener) {
    if (!this.assertedQueues.includes(name)) {
      this.channel.assertQueue(name, { durable: true });
    }
    this.channel.consume(name, (msg) => listener(JSON.parse(msg.content.toString())), { noAck: true });
  }

  addSubscribeToConn(name, listener) {
    if (!this.assertedExchanges.includes(name)) {
      this.channel.assertExchange(name, 'fanout', { durable: false });
    }

    this.channel.assertQueue('', { exclusive: true }, (err, q) => {
      this.channel.bindQueue(q.queue, name, '');

      this.channel.consume(q.queue, (msg) => listener(JSON.parse(msg.content.toString())), { noAck: true });
    });
  }

  queue(name, listener) {
    this.queueListeners.push([name, listener]);

    if (this.channel) {
      this.addQueueToConn(name, listener);
    }
  }

  subscribe(name, listener) {
    this.subscribeListeners.push([name, listener]);

    if (this.channel) {
      this.addSubscribeToConn(name, listener);
    }
  }

  sendToQueue(name, msg) {
    if (this.channel) {
      if (!this.assertedQueues.includes(name)) {
        this.channel.assertQueue(name, { durable: true });
        this.assertedQueues.push(name);
      }

      this.channel.sendToQueue(name, new Buffer(JSON.stringify(msg)), { persistent: true });
    } else {
      this.once('connected', this.sendToQueue.bind(this, name, msg));
    }
  }

  publish(name, msg) {
    if (this.channel) {
      if (!this.assertedExchanges.includes(name)) {
        this.channel.assertExchange(name, 'fanout', { durable: false });
        this.assertedExchanges.push(name);
      }

      this.channel.publish(name, '', new Buffer(JSON.stringify(msg)));
    } else {
      this.once('connected', this.publish.bind(this, name, msg));
    }
  }
}

AMQP.default = AMQP; // ES6 module support
module.exports = AMQP;

